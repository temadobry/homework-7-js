// 
// 2.Как очистит массив? Указать длинну массива 0 arr.length = 0
//  
//  
//  

"use strict"

let filterBy = (newArr, newdataType) => {
  if (newdataType === "array") 
    return newArr.filter(elem => !Array.isArray(elem));
  else if (newdataType === 'null') 
    return newArr.filter(elem => elem !== null);
  else if (newdataType === 'object') 
    return newArr.filter(elem => newdataType !== (typeof elem) || Array.isArray(elem) || elem === null)
  else
    return newArr.filter(elem => typeof elem !== newdataType);
}

let user = { name: "John", age: 30 }
const arr = ['hello', 'world', 23, '23', [1, 2, 3, 4], user, null]
const dataType = 'null'
const dataType2 = 'array'
const dataType3 = 'object'
const dataType4 = 'string'
const dataType5 = 'number'
  

console.log(filterBy(arr, dataType));
console.log(filterBy(arr, dataType2));
console.log(filterBy(arr, dataType3));
console.log(filterBy(arr, dataType4));
console.log(filterBy(arr, dataType5));























